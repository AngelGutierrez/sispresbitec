	<?php 
		if ( ! function_exists('cargar_headers')){
			function cargar_headers(){
				$headers = "
					<head>
						<meta charset='utf-8'>
						<title></title>
						<style type='text/css'></style>
						<link type='text/css' rel='stylesheet' href='css/estilos_general.css'/>
						<link type='text/css' rel='stylesheet' href='".base_url()."css/material-icons/css/materialicons.css'/>
						<link type='text/css' rel='stylesheet' href='".base_url()."libs/materialize/css/materialize.min.css'/>
						<link rel='stylesheet' type='text/css' href='".base_url()."libs/jq-datatable/datatables.min.css'/>
						<script type='text/javascript' src='".base_url()."libs/jquery/jquery-3.2.1.min.js'></script>
					    <script type='text/javascript' src='".base_url()."libs/materialize/js/materialize.min.js'></script>


					    <link type='text/css' rel='stylesheet' href='".base_url()."libs/sweetalert.css'/>
						<script src='".base_url()."libs/sweetalert.min.js'</script>


					    <script src='".base_url()."libs/jq-datatable/datatables.min.js'></script>
						<script src='".base_url()."libs/jq-datatable/js/dataTables.semanticui.min.js'></script>
						<img class='center-align' src='imgs/cintillo.png' width='1100' height='70'>
					</head>";
				return $headers;
			}
    	}
		if ( ! function_exists('mostrar_nav_bar')){
			function mostrar_nav_bar(){
								//<a href='#!user'><img class='circle' src='".base_url()."imgs/user.jpeg'></a>
				$html_navBar = "<div>
					<ul id='slide-out' class='side-nav fixed'>
						<li>
							<div class='user-view'>
					      		<div class='background' style='background-color:  #35060b;'>
					        		<img src='' >
					      		</div>
								<a href='#!name'><span class='white-text name'>Angel Gutierrez</span></a>
								<a href='#!email'><span class='white-text email'>angelgutierrez1983@gmail.com</span></a>
				    		</div>
				    	</li>
				    	<li><a href='".site_url()."/documento'><i style='font-size: 25px' class='material-icons'>Inicio</i>Inicio</a></li>
				    	
						<li class='no-padding'>
					        <ul class='collapsible collapsible-accordion'>
					          	<li>
					            	<a class='collapsible-header'>
					            	Administracion<i class='material-icons'>arrow_drop_down</i></a>
						            <div class='collapsible-body'>
						              	<ul>
							                <li><a href='".base_url()."administracion_controller/listarUsuarios'>Usuarios</a></li>
						              	</ul>
						            </div>
					          	</li>

					          	<li>
					            	<a class='collapsible-header'>Inventario<i class='material-icons'>arrow_drop_down</i></a>
						            <div class='collapsible-body'>
						              	<ul>
							                <li><a href='".base_url()."administracion_controller/listarBienesTecnologicos'>Bienes Tecnologico</a></li>

							                <li><a href='".base_url()."administracion_controller/listarConsumibles'>Consumibles</a></li>
						              	</ul>
						            </div>
					          	</li>

					          	<li>
					            	<a class='collapsible-header'>Movimientos<i class='material-icons'>arrow_drop_down</i></a>
						            <div class='collapsible-body'>
						              	<ul>
							                <li><a href='#!'>Solicitud</a></li>
							                <li><a href='#!'>Asignaciones</a></li>
							                <li><a href='#!'>Devoluciones</a></li>
						              	</ul>
						            </div>
					          	</li>

					          	<li>
					            	<a class='collapsible-header'>Reportes<i class='material-icons'>arrow_drop_down</i></a>
						            <div class='collapsible-body'>
						              	<ul>
							                <li><a href='#!'>Solicitud</a></li>
							                <li><a href='#!'>Asignaciones</a></li>
							                <li><a href='#!'>Devoluciones</a></li>
						              	</ul>
						            </div>
					          	</li>

					          	<li>
					            	<a class='collapsible-header'>Estadisticas<i class='material-icons'>arrow_drop_down</i></a>
						            <div class='collapsible-body'>
						              	<ul>
							                <li><a href='#!'>Solicitud</a></li>
							                <li><a href='#!'>Asignaciones</a></li>
							                <li><a href='#!'>Devoluciones</a></li>
						              	</ul>
						            </div>
					          	</li>
					        </ul>
				        </li>
				    </ul>
			    </div>";
			    return $html_navBar;
			}
    	}
    	if ( ! function_exists('mostrar_footer')){
			function mostrar_footer(){
		    	$footer = "
		    		<div  style='padding-top:2.0em;'>
				    	<footer class='page-footer'>
							<div class='footer-copyright'>
								<div class='container'>
									© 2016 Copyright Information
									<a class='grey-text text-lighten-4 right' href='#!'>Links</a>
								</div>
				          	</div>         
				       	</footer>
			       	</div>
		       	";
		       	return $footer;
   			}
		}
		if( ! function_exists( 'autocodigo' ) ) {
			function autocodigo( $tabla, $id, $parametros=array() ){
			  	$prefijo_tabla="";
			  	$ceros=4;
				extract($parametros);		
				$salida = "";

				//se separa el esquema
				$parte1 = explode( '.', $tabla );
				$tabla_sola = isset( $parte1[1] )? $parte1[1] : $tabla;

				$parte2 = explode( '_', $tabla_sola );
				array_shift( $parte2 );
				if($prefijo_tabla==""){
					$prefijo_tabla = substr( $parte2[0], 0, 2);
					if ( count( $parte2 ) > 1 ){
						$prefijo_tabla  = substr( $parte2[0], 0, 1);
						$prefijo_tabla .= substr( $parte2[1], 0, 1);
					}
				}
				$autocodigo = strtoupper( $prefijo_tabla ) . sprintf("%0".$ceros."s", $id);
				$salida = $autocodigo;

				return $salida;
			}
		}

		if( ! function_exists('prp'))
		{
			function prp($arg=array())
			{
				echo "\n<pre>\n";
				if( ! is_array($arg)) {
					if( ! is_object($arg)) {
						echo $arg;
					} else {
						print_r((array) $arg);
					}
				} else {
					print_r($arg);
				}
				echo "</pre>\n";
			}
}
    ?>