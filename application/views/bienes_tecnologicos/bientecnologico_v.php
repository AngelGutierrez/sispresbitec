<!DOCTYPE html>
<html>
    <head>
        
        <?=cargar_headers()?>
        <?=mostrar_nav_bar()?>
        <meta charset="utf-8" />
        <style type="text/css">
            body{
                margin-left: 304px; 
            }
            .waves-effect{
                margin-left: 10px;
            }
            .input-field{
                margin-top: 1px;
            }
        </style>
    </head>
    <body>
    <div class="row">
        <h4 align="center"> <?= $titulo ?> </h4>
        <form method="POST" id="formRegistro" name="formRegsitro" action="<?= site_url()?>/administracion_controller/guardarBienTecnologico/"/>
        <div class="col s6 m6 l6">
            <a class="waves-effect waves-light btn"><i class="material-icons left">reply</i>Volver</a>      
        </div>
        <div class="col s6 m6 l6 right-align">
        <a class="waves-effect waves-light btn" onclick="guardar()" ><i class="material-icons left">save</i>Guardar</a>            
        </div>
            <div class="col s4 m4 l4">
                Serial: 
                <input type="text" name="serial" id="serial">  
            </div>
            <div class="col s4 m4 l4">
                Marca: 
                <input type="text" name="marca" id="marca">  
            </div>
            <div class="col s4 m4 l4">
                Modelo: 
                <input type="text" name="modelo" id="modelo">  
            </div>
            <div class="col s4 m4 l4">
                Placa de bienes: 
                <input type="text" name="placa_bienes" id="placa_bienes">  
            </div>
            <div class="col s4 m4 l4">
                Cantidad: 
                <input type="number" name="cantidad" id="cantidad" min="0" max="100">  
            </div>
            <div class="col s4 m4 l4">
                Descripcion: 
                <input type="text" name="descripcion" id="descripcion">  
            </div>
            <div class="col m4 m4 l4">
                Tipo de bien tecnologíco 
                <select name="tipo_bien_tecnologico" class="input-field col s12" id="tipoBienTecnologico">
                    <option value="TBT001">Laptop</option>
                    <option value="TBT002">Video beam</option>
                    <option value="TBT003">Unidad de disco Duro</option>
                    <option value="TBT004">Unidad de DVD</option>
                </select>                
            </div>
            <div class="col s4 m4 l4">
                Estatus del Bien Tecnologico: 
                <select name="codigo_estatus_bt" class="input-field col s12">
                   <option value="EST001">Activo</option>
                   <option value="EST002">En revision</option>
                   <option value="EST003">Desincorporado</option>
                </select>
            </div>
            <div class="col s4 m4 l4">
                Disponibilidad del Bien Tecnologico: 
                <select name="codigo_disponibilidad_bt" class="input-field col s12">
                   <option value="CD001">Disponible</option>
                   <option value="CD002">Reservado</option>
                </select>
            </div>
            <div class="col s4 m4 l4">
                Codigo Bien  Tecnologico: 
                <input type="text" name="codigo_bien_tecnologico" id="codigo_bien_tecnologico">  
            </div> 
        
       </from>
    </div>
     <script type="text/javascript">
         
       $(document).ready(function() {
        $('select').material_select();
      });
            
    
    function guardar(){
        //alert('a')
        $('#formRegistro').submit();
    }
     </script>

    </body>
</html>


        
        




