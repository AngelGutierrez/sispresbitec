<!DOCTYPE html>
<html>
<head>
	<?=cargar_headers()?>
	<?=mostrar_nav_bar()?>
	<meta charset="utf-8" />
	<script type="text/javascript" src="<?= base_url()?>plantilla/js/jquery.js"></script>
	<style type="text/css">
		body{
			margin-left: 304px; 
		}
		.waves-effect{
			margin-left: 10px;
		}
	</style>
</head>

<body>
	<!--?=  //prp($bienes); ?-->
		<div align="center">
			<h1>Lista De Bienes Tecnologicos</h1><br><br>			
		</div>
		<hr>
		<a class="waves-effect waves-light btn" href='registroBT/n/'><i class="material-icons left">add</i>Nuevo</a>
		<a class="waves-effect waves-light btn"><i class="material-icons left">find_in_page</i>Filtrar</a>
		<hr>

		<table class="col center-aling s12 m12 l12">
			<tr>
				<td>
					Marca
				</td>
				<td>
					Modelo
				</td>
				<td>
					Disponibilidad
				</td>
				<td>
					Tipo Bien Tecnologico
				</td>
				<td>
					Accion
				</td>
			</tr>
			<?php 
				foreach ( $bienes as $bien) { 
					echo "<tr>";
					echo "<td>".$bien['marca']."</td>";
					echo "<td>".$bien['modelo']."</td>";
					echo "<td>".$bien['descripcion_estatus_bien_tecnologico']."</td>";
					echo "<td>".$bien['descripcion_bien_tecnologico']."</td>";
					//echo "<td>".$bien['serial']."</td>";
					echo "<td align='center'><a href='registroBT/e/".$bien['serial']."'><i class='material-icons left'>mode_edit</i></a> 
										     <a href='eliminar/x/'><i class='material-icons left'>delete</i></a></td>";
					echo "</tr>";
					//echo $persona->usuario."<br />";
				}
			?>
		</table>

	</form>
	<?=validation_errors();?>
	<script>
	
	</script>
</body>

</html>